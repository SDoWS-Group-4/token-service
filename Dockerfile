# Multistage java creation Dockerfile
# 1. Build java program and package as jar 
# FROM maven:3.6.0-jdk-11-slim AS builder
# COPY features /home/app/features
# COPY src /home/app/src
# COPY pom.xml /home/app
# RUN mvn -f /home/app/pom.xml clean package

## 2. Copy created jar to seperate container without dependencies 
#FROM openjdk:11.0.1-jdk-slim
##COPY --from=builder /home/app/target/TokenService.jar /usr/local/lib/TokenService.jar
#COPY target/TokenService.jar /usr/local/lib/TokenService.jar
#ENTRYPOINT ["java","-jar","/usr/local/lib/TokenService.jar"]
FROM adoptopenjdk:11-jre-hotspot
COPY target/lib /usr/src/lib
COPY target/token-service-1.0.0.jar /usr/src/
WORKDIR /usr/src/
CMD java -Xmx64m -jar token-service-1.0.0.jar

