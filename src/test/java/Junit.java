import dtu.TokenStore;
import dtu.manager.TokenManager;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * A test class to check which tests are run with which framework.
 * If run as a JUnit 5 (Jupiter) test, then both tests are run.
 * If run as a JUnit 4 test, then only the JUnit 4 tests are run.
 */
public class Junit {

    @org.junit.Test // Junit 4
    public void setUp() {
        int length = 25;
        boolean useLetters = true;
        boolean useNumbers = true;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);

        System.out.println(generatedString);
    }

    @org.junit.Test // Junit 4
    public void removeToken() {
        HashMap<String, List<String>> tokens = new HashMap<>();
        TokenStore tokenStore = TokenStore.getInstance();
        TokenManager tokenManager = new TokenManager();
        String id = "123456789";

        tokenStore.setTokens(id, new ArrayList<>(Arrays.asList("1251afd67788", "23616172sdf1")));
        System.out.println(tokenStore.getTokensById(id));

        tokenManager.consumeToken(id,"1251afd67788");
        System.out.println(tokenStore.getTokensById(id));
    }

    @org.junit.Test // JUnit 4
    public void junit4Test() {
//        System.out.println("JUnit 4");

        TokenStore tokenStore = TokenStore.getInstance();
        tokenStore.setTokens("123456",  new ArrayList<>(Arrays.asList("1251afd67788")));
        System.out.println(tokenStore.getAccountByToken("1251afd67788"));
        org.junit.Assert.assertTrue(true); // JUnit 4
    }

}
