package dtu.model;

import lombok.Value;

import java.util.UUID;

@Value
public class CorrelationId {
	private UUID id;

	public CorrelationId(UUID randomUUID) {
		id = randomUUID;
	}

	public static CorrelationId randomId() {
		return new CorrelationId(UUID.randomUUID());
	}
}
