package dtu.service;


import dtu.manager.TokenManager;
import dtu.model.CorrelationId;
import dtu.model.Customer;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class AccountService {

    MessageQueue queue;
    private Map<CorrelationId, CompletableFuture<Customer>> correlations = new ConcurrentHashMap<>();

    public AccountService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("CustomerByIdFound", this::handleFoundAccount);
        this.queue.addHandler("CustomerByIdNotFound", this::handleNotFoundAccount);
    }

    public Customer getAccount(String id) throws Exception {
        try {
            var correlationId = CorrelationId.randomId();
            correlations.put(correlationId, new CompletableFuture<>());   // check correlation expected response
            Event event = new Event("CustomerByIdRequested", new Object[] {id, correlationId});
            queue.publish(event);
            return correlations.get(correlationId).join();    // If account is not empty
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getCause() != null) {
                if (e.getCause().getClass() == IllegalArgumentException.class) {
                    throw new IllegalArgumentException(e.getCause().getMessage());
                }
            }
            throw new Exception(e.getMessage());
        }
    }

    public void handleFoundAccount(Event e) {
        var customer = e.getArgument(0, Customer.class);
        var cid = e.getArgument(1, CorrelationId.class);

        correlations.get(cid).complete(customer);
    }


    private void handleNotFoundAccount(Event e) {
        Exception exception = e.getArgument(0, Exception.class);
        var cid = e.getArgument(1, CorrelationId.class);
        // correlations.get(cid).complete(exception);
        correlations.get(cid).completeExceptionally(exception);
    }
}
