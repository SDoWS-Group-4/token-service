package dtu.service;


import java.util.ArrayList;

import dtu.manager.TokenManager;
import dtu.model.CorrelationId;
import dtu.model.Customer;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class TokenService {

    MessageQueue queue;
    TokenManager tokenManager = new TokenManager();
    private Map<CorrelationId, CompletableFuture<List<String>>> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, CompletableFuture<Customer>> correlationsAcc = new ConcurrentHashMap<>();


    public TokenService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("ValidateToken", this::handleValidateTokenRequest);
        this.queue.addHandler("ConsumeToken", this::handleConsumeTokenRequest);
        this.queue.addHandler("GenerateTokenRequest", this::handleGenerateTokensRequest);
    }

    public void handleValidateTokenRequest(Event ev) {     // What shoud this be responsible for?
        var token = ev.getArgument(0, String.class); //Getting token
        var correlationId = ev.getArgument(1, CorrelationId.class);
        Event event;

        try {
            // Checking if id has given token and returning bool
            String DTUaccount = tokenManager.validateToken(token);
            event = new Event("TokenValidated", new Object[]{DTUaccount, correlationId});
        } catch (Exception e) {
            event = new Event("TokenNotValidated", new Object[]{e, correlationId});
        }
        queue.publish(event);
    }

    public void handleConsumeTokenRequest(Event ev) {     // What shoud this be responsible for?
        var id = ev.getArgument(0, String.class); //Getting id
        var token = ev.getArgument(1, String.class); //Getting id
        var correlationId = ev.getArgument(2, CorrelationId.class);
        Event event;

        try {
            tokenManager.consumeToken(id,token);
            // Checking if id has given token and returning bool
            event = new Event("TokenConsumed", new Object[] {"Token consumed", correlationId });
        } catch (Exception e) {
            event = new Event("TokenNotConsumed", new Object[] {e , correlationId });
        }
        queue.publish(event);
    }

    public void handleGenerateTokensRequest(Event ev) {     // What shoud this be responsible for?

        var id = ev.getArgument(0, String.class); //Getting id
        var quantity = ev.getArgument(1, String.class); //Getting id
        var correlationId = ev.getArgument(2, CorrelationId.class);
        Event event;

        try {
            ArrayList<String> genToken = tokenManager.generateTokens(id, Integer.parseInt(quantity));

            event = new Event("TokensGenerated", new Object[] { genToken, correlationId });
        } catch (IllegalArgumentException e) {

            event = new Event("TokensNotGenerated", new Object[] {e , correlationId });
        }
        queue.publish(event);
    }

    public Customer getAccount(String id) throws Exception {
        try {
            var correlationId = CorrelationId.randomId();
            correlationsAcc.put(correlationId, new CompletableFuture<>());   // check correlation expected response
            Event event = new Event("MerchantByIdRequested", new Object[] {id, correlationId});
            queue.publish(event);
            return correlationsAcc.get(correlationId).join();    // If account is not empty
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getCause() != null) {
                if (e.getCause().getClass() == IllegalArgumentException.class) {
                    throw new IllegalArgumentException(e.getCause().getMessage());
                }
            }
            throw new Exception(e.getMessage());
        }
    }
}
