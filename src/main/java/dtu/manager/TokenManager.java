package dtu.manager;

import dtu.TokenStore;
import dtu.service.AccountService;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.ArrayList;

public class TokenManager {

    TokenStore tokenStore = TokenStore.getInstance();
    AccountService accountService = new AccountServiceFactory().getService();

    public ArrayList<String> generateTokens(String id, int quantity) { // Generating a random string for token
        boolean useNumbers = true;
        boolean useLetters = true;
        int length = 30;

        //Check if id exists first


        if (tokenStore.getTokensById(id).size() > 1) {throw new IllegalArgumentException("Customer has too many tokens");}
        if (tokenStore.getTokensById(id).size() + quantity > 6) {throw new IllegalArgumentException("Customer is not allowed more than 6 tokens");}

        ArrayList<String> tokenList = tokenStore.getTokensById(id); // Getting existing tokens if exist

        for (int i = 0; i < quantity; i++) {
            String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
            System.out.println(generatedString);
            tokenList.add(generatedString);
        }

        tokenStore.setTokens(id, tokenList); // Generated tokens to the user
        return tokenStore.getTokensById(id);
    }

    public String validateToken(String token) throws Exception {

        //If token exists get id
        String DTUaccount = tokenStore.getAccountByToken(token);

        try{
            accountService.getAccount(DTUaccount);
        }catch (Exception e){
            System.out.println(e.getMessage());
            throw new Exception("Account is not valid");
        }
        /*if (accountService.getAccount(DTUaccount) == null) { // if account is not null
            throw new Exception("Account is not valid");
        }*/

        if (!DTUaccount.isEmpty()) { //If account was found
            consumeToken(DTUaccount,token);
            return DTUaccount;
        } else {
            throw new Exception("Token is not valid");
        }
    }


    public void consumeToken(String id, String token) {
        tokenStore.getTokensById(id).remove(token);
    }

    public void deleteUser(String id) {
        tokenStore.deleteTokenUser(id);
    }

}
