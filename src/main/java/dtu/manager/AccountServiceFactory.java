package dtu.manager;

import dtu.service.AccountService;
import dtu.service.TokenService;
import messaging.implementations.RabbitMqQueue;

public class AccountServiceFactory {
    static AccountService service = null;

	public AccountService getService() {

		if (service != null) {
			return service;
		}
		var mq = new RabbitMqQueue("rabbitmq");
		service = new AccountService(mq);
		return service;
	}
}
