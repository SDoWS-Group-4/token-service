package dtu;

import java.util.*;


public class TokenStore {

    private HashMap<String, ArrayList<String>> tokens = new HashMap<>();
    private static TokenStore single_instance = null;

    public TokenStore() {
    }

    public HashMap<String, ArrayList<String>> getTokens() {
        return tokens;
    }

    public void setTokens(String id, ArrayList<String> tokens) {
        this.tokens.put(id, tokens);
    }

    public ArrayList<String> getTokensById(String id) {
        tokens.computeIfAbsent(id, k -> new ArrayList<String>());
        return tokens.get(id);
    }

    public static TokenStore getInstance() {
        if (single_instance == null)
            single_instance = new TokenStore();

        return single_instance;
    }

    public HashMap<String, ArrayList<String>> getAllTokens () {
        return tokens;
    }

    public String getAccountByToken(String token) {
        String accountId = "";
        for(Map.Entry<String,ArrayList<String>> entry: tokens.entrySet()){
            if (entry.getValue().contains(token)) {
                accountId = entry.getKey();
                break;
            }
        }
        return accountId;
    }

    public void deleteTokenUser(String id) {
        tokens.get(id).clear();
    }

    public void addTokenUserWithTokens(String id, int amountOfTokens) {
        ArrayList<String> tokens = new ArrayList<String>();
        
        for (int i = 0; i < amountOfTokens; i++ ){
            tokens.add(UUID.randomUUID().toString());
        }
        this.tokens.put(id, tokens);
    }
}